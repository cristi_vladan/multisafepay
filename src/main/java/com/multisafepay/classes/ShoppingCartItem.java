package com.multisafepay.classes;

public class ShoppingCartItem {
	private String name 				= null;
	private String description 		= null;
	private String unit_price 		= null;
	private String currency 			= null;
	private String quantity 			= null;
	private String merchant_item_id 	= null;
	private String tax_table_selector= null;
	private Weight weight 			= null;
	
	
	
	public static  ShoppingCartItem add(
			String name,
			String description, 
			String unit_price,
			String currency,
			String quantity,
			String merchant_item_id,
			String tax_table_selector, 
			Weight weight)
	{
		ShoppingCartItem item	=	new ShoppingCartItem();

		item.name 				= name;
		item.description 		= description;
		item.unit_price 		= unit_price;
		item.currency 			= currency;
		item.quantity 			= quantity;
		item.merchant_item_id 	= merchant_item_id;
		item.tax_table_selector	= tax_table_selector;
		item.weight 			= weight;
		return item;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(String unit_price) {
		this.unit_price = unit_price;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getMerchant_item_id() {
		return merchant_item_id;
	}

	public void setMerchant_item_id(String merchant_item_id) {
		this.merchant_item_id = merchant_item_id;
	}

	public String getTax_table_selector() {
		return tax_table_selector;
	}

	public void setTax_table_selector(String tax_table_selector) {
		this.tax_table_selector = tax_table_selector;
	}

	public Weight getWeight() {
		return weight;
	}

	public void setWeight(Weight weight) {
		this.weight = weight;
	}
}


