package com.multisafepay.classes;

public class CheckoutOptions {

	private TaxTables tax_tables 				= null;
	private ShippingMethods shipping_methods 	= null;
	private RoundingPolicy rounding_policy 		= null;
	private String no_shipping_method 			= null;
	private Boolean use_customfield_notification	= null;
	private Boolean use_shipping_notification	= null;

	public TaxTables getTax_tables() {
		return tax_tables;
	}

	public void setTax_tables(TaxTables tax_tables) {
		this.tax_tables = tax_tables;
	}

	public ShippingMethods getShipping_methods() {
		return shipping_methods;
	}

	public void setShipping_methods(ShippingMethods shipping_methods) {
		this.shipping_methods = shipping_methods;
	}

	public RoundingPolicy getRounding_policy() {
		return rounding_policy;
	}

	public void setRounding_policy(RoundingPolicy rounding_policy) {
		this.rounding_policy = rounding_policy;
	}

	public String getNo_shipping_method() {
		return no_shipping_method;
	}

	public void setNo_shipping_method(String no_shipping_method) {
		this.no_shipping_method = no_shipping_method;
	}

	public Boolean getUse_customfield_notification() {
		return use_customfield_notification;
	}

	public void setUse_customfield_notification(Boolean use_customfield_notification) {
		this.use_customfield_notification = use_customfield_notification;
	}

	public Boolean getUse_shipping_notification() {
		return use_shipping_notification;
	}

	public void setUse_shipping_notification(Boolean use_shipping_notification) {
		this.use_shipping_notification = use_shipping_notification;
	}
}
