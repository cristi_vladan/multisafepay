package com.multisafepay.classes;

public class Order {

	private String order_id			= null;
	private String recurring_id		= null;
	private String cost				= null;
	private String type				= null;
	private String currency			= null;
	private Integer amount			= null;
	private String description		= null;
	private String manual			= null;
	private String gateway			= null;
	private String days_active		= null;
	private String payment_url		= null;
	private String ship_date			= null;
	private String tracktrace_code	= null;
	private String reason			= null;
	private String carrier			= null;
	private String invoice_id		= null;
	private String items				= null;
	private String cart_expiration	= null;

	private String var1				= null;
	private String var2				= null;
	private String var3				= null;
	
	private String use_shipping_notification	= null;
	private String use_field_notifications 	= null;
	
	private Customer customer 				= null;
	private Delivery delivery 				= null;
	private Plugin plugin 					= null;
	private GatewayInfo gateway_info 		= null;
	private PaymentOptions payment_options 	= null;
	private ShoppingCart shopping_cart 		= null;
	private GoogleAnalytics google_analytics = null;
	private CustomFields custom_fields		= null;
	private CustomInfo custom_info			= null;
	private CheckoutOptions checkout_options	= null;
	
	private Order setRedirect(String order_id,String description,Integer amount,String currency,PaymentOptions payment_options)
	{
		this.type 				= "redirect";
		this.order_id 			= order_id;
		this.description 		= description;
		this.amount 			= amount;
		this.currency 			= currency;
		this.payment_options 	= payment_options;
		return this;
	}
	
	private Order setFastCheckout(String order_id,String description,Integer amount,String currency,PaymentOptions payment_options,ShoppingCart shopping_cart,CheckoutOptions checkout_options)
	{
		this.type 				= "checkout";
		this.order_id 			= order_id;
		this.description 		= description;
		this.amount 			= amount;
		this.currency 			= currency;
		this.payment_options 	= payment_options;
		this.shopping_cart 		= shopping_cart;
		this.checkout_options 	= checkout_options;
		return this;
	}
	
	private Order setDirectPayAfter(String order_id,String description,Integer amount,String currency,PaymentOptions payment_options,GatewayInfo gateway_info,ShoppingCart shopping_cart,CheckoutOptions checkout_options)
	{
		this.type 				= "direct";
		this.gateway 			= "PAYAFTER";
		this.order_id 			= order_id;
		this.description 		= description;
		this.amount 			= amount;
		this.currency 			= currency;
		this.payment_options 	= payment_options;
		this.shopping_cart 		= shopping_cart;
		this.checkout_options 	= checkout_options;
		return this;
	}
	
	private Order setDirectBank(String order_id,String description,Integer amount,String currency,GatewayInfo gateway_info)
	{
		this.type 				= "direct";
		this.gateway			= "DIRECTBANK";
		this.order_id 			= order_id;
		this.description 		= description;
		this.amount 			= amount;
		this.currency 			= currency;
		this.gateway_info 		= gateway_info;
		return this;
	}
	
	private Order setDirectIdeal(String order_id,String description,Integer amount,String currency,PaymentOptions payment_options,GatewayInfo gateway_info)
	{
		this.type 				= "direct";
		this.gateway			= "IDEAL";
		this.order_id 			= order_id;
		this.description 		= description;
		this.amount 			= amount;
		this.currency 			= currency;
		this.payment_options 	= payment_options;
		this.gateway_info 		= gateway_info;
		return this;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getRecurring_id() {
		return recurring_id;
	}

	public void setRecurring_id(String recurring_id) {
		this.recurring_id = recurring_id;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getManual() {
		return manual;
	}

	public void setManual(String manual) {
		this.manual = manual;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getDays_active() {
		return days_active;
	}

	public void setDays_active(String days_active) {
		this.days_active = days_active;
	}

	public String getPayment_url() {
		return payment_url;
	}

	public void setPayment_url(String payment_url) {
		this.payment_url = payment_url;
	}

	public String getShip_date() {
		return ship_date;
	}

	public void setShip_date(String ship_date) {
		this.ship_date = ship_date;
	}

	public String getTracktrace_code() {
		return tracktrace_code;
	}

	public void setTracktrace_code(String tracktrace_code) {
		this.tracktrace_code = tracktrace_code;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getInvoice_id() {
		return invoice_id;
	}

	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}

	public String getItems() {
		return items;
	}

	public void setItems(String items) {
		this.items = items;
	}

	public String getCart_expiration() {
		return cart_expiration;
	}

	public void setCart_expiration(String cart_expiration) {
		this.cart_expiration = cart_expiration;
	}

	public String getVar1() {
		return var1;
	}

	public void setVar1(String var1) {
		this.var1 = var1;
	}

	public String getVar2() {
		return var2;
	}

	public void setVar2(String var2) {
		this.var2 = var2;
	}

	public String getVar3() {
		return var3;
	}

	public void setVar3(String var3) {
		this.var3 = var3;
	}

	public String getUse_shipping_notification() {
		return use_shipping_notification;
	}

	public void setUse_shipping_notification(String use_shipping_notification) {
		this.use_shipping_notification = use_shipping_notification;
	}

	public String getUse_field_notifications() {
		return use_field_notifications;
	}

	public void setUse_field_notifications(String use_field_notifications) {
		this.use_field_notifications = use_field_notifications;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public Plugin getPlugin() {
		return plugin;
	}

	public void setPlugin(Plugin plugin) {
		this.plugin = plugin;
	}

	public GatewayInfo getGateway_info() {
		return gateway_info;
	}

	public void setGateway_info(GatewayInfo gateway_info) {
		this.gateway_info = gateway_info;
	}

	public PaymentOptions getPayment_options() {
		return payment_options;
	}

	public void setPayment_options(PaymentOptions payment_options) {
		this.payment_options = payment_options;
	}

	public ShoppingCart getShopping_cart() {
		return shopping_cart;
	}

	public void setShopping_cart(ShoppingCart shopping_cart) {
		this.shopping_cart = shopping_cart;
	}

	public GoogleAnalytics getGoogle_analytics() {
		return google_analytics;
	}

	public void setGoogle_analytics(GoogleAnalytics google_analytics) {
		this.google_analytics = google_analytics;
	}

	public CustomFields getCustom_fields() {
		return custom_fields;
	}

	public void setCustom_fields(CustomFields custom_fields) {
		this.custom_fields = custom_fields;
	}

	public CustomInfo getCustom_info() {
		return custom_info;
	}

	public void setCustom_info(CustomInfo custom_info) {
		this.custom_info = custom_info;
	}

	public CheckoutOptions getCheckout_options() {
		return checkout_options;
	}

	public void setCheckout_options(CheckoutOptions checkout_options) {
		this.checkout_options = checkout_options;
	}
}
