package com.multisafepay.classes;

public class Customer {

	private String locale 			= null;
	private String ip_address 		= null;
	private String forwarded_ip 		= null;
	private String first_name 		= null;
	private String last_name 		= null;
	private String address1 			= null;
	private String address2 			= null;
	private String house_number 		= null;
	private String zip_code 			= null;
	private String city 				= null;
	private String state 			= null;
	private String country 			= null;
	private String phone 			= null;
	private String email 			= null;
	private String referrer 			= null;
	private String user_agent 		= null;
	private Boolean company 			= null;
	private Boolean close_window 	= null;
	private Boolean disable_send_email = null;

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getIp_address() {
		return ip_address;
	}

	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	public String getForwarded_ip() {
		return forwarded_ip;
	}

	public void setForwarded_ip(String forwarded_ip) {
		this.forwarded_ip = forwarded_ip;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getHouse_number() {
		return house_number;
	}

	public void setHouse_number(String house_number) {
		this.house_number = house_number;
	}

	public String getZip_code() {
		return zip_code;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public String getUser_agent() {
		return user_agent;
	}

	public void setUser_agent(String user_agent) {
		this.user_agent = user_agent;
	}

	public Boolean getCompany() {
		return company;
	}

	public void setCompany(Boolean company) {
		this.company = company;
	}

	public Boolean getClose_window() {
		return close_window;
	}

	public void setClose_window(Boolean close_window) {
		this.close_window = close_window;
	}

	public Boolean getDisable_send_email() {
		return disable_send_email;
	}

	public void setDisable_send_email(Boolean disable_send_email) {
		this.disable_send_email = disable_send_email;
	}
}
