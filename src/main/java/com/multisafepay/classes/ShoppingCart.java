package com.multisafepay.classes;

import java.util.List;

public class ShoppingCart {
	private List<ShoppingCartItem> items 	= null;

    public ShoppingCart() {}

	public ShoppingCart(List<ShoppingCartItem> items)
	{
		this.items	=  items;
	}


    public List<ShoppingCartItem> getItems() {
        return items;
    }

    public void setItems(List<ShoppingCartItem> items) {
        this.items = items;
    }
}
